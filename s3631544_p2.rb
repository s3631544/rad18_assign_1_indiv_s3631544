require 'nokogiri'
require 'json'

class Function

    def read_file(file_name)
        puts file_name
        xmlfile = File.open(file_name)
        $doc = Nokogiri::XML.parse(xmlfile)
        
    end

    def help
        puts "list [--option *search_term*]   -key word search using either ip or name. Replace option with either ip or name"
        puts "-xml [filename]   -load the xml file into ruby "
    end

    def listing_record(options,key)
        if options == "--name"
            $record.search_first_name(key)
        elsif options == "--ip"
            $record.search_ip_address(key)
        end
    end

end

class Record

        def search_first_name(name)
            $doc.xpath('//record').each do 
                |record_element|
                    if record_element.xpath('first_name').text.downcase == name.chomp.downcase
                        record_output = {
                        :Id =>record_element.xpath('id').text,
                        :First_name  => record_element.xpath('first_name').text, 
                        :Last_name => record_element.xpath('last_name').text,
                        :Email => record_element.xpath('email').text,
                        :Gender =>record_element.xpath('gender').text,
                        :Ip_address =>record_element.xpath('ip_address').text,
                        :Send_date =>+record_element.xpath('send_date').text,
                        :Email_body =>+record_element.xpath('email_body').text,
                        :Email_title =>+record_element.xpath('email_title').text
                        }
                        puts JSON.pretty_generate(record_output)
                    end
                end 
        end
    
        def search_ip_address(name)
            $doc.xpath('//record').each do 
                |record_element|
                    if record_element.xpath('ip_address').text == name.chomp
                        record_output = {
                        :Id =>record_element.xpath('id').text,
                        :First_name  => record_element.xpath('first_name').text, 
                        :Last_name => record_element.xpath('last_name').text,
                        :Email => record_element.xpath('email').text,
                        :Gender =>record_element.xpath('gender').text,
                        :Ip_address =>record_element.xpath('ip_address').text,
                        :Send_date =>+record_element.xpath('send_date').text,
                        :Email_body =>+record_element.xpath('email_body').text,
                        :Email_title =>+record_element.xpath('email_title').text
                        }
                        puts JSON.pretty_generate(record_output)
                    end
                end 
        end
    end

option = ARGV[0]
function = Function.new
$record = Record.new
if option == "-xml"
    filename = "nothing"
    if ARGV[1]== nil
        filename = "emails.xml"
    else
        filename = ARGV[1]
    end 
    function.read_file(filename)
elsif option == "list"
    xmlfile = File.read(file)
    $doc = Nokogiri::XML.parse(xmlfile)
    function.listing_record(ARGV[1],ARGV[2])
elsif option == "help"
    function.help
elsif option == nil
    puts "Commands"
    puts "s123456_p2.rb -xml[filename]"
    puts "s123456_p2.rb help"
end