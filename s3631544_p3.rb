require 'nokogiri'
require 'json'
require 'date'

class Function

    def read_file(file_name)

        puts file_name
        xmlfile = File.read(file_name)
        $doc = Nokogiri::XML.parse(xmlfile)
        
    end

    def help
        puts "list [--option] [*search_term*]   -key word search using either ip or name. Replace option with either ip or name"
        puts "-xml [filename]   -load the xml file into ruby "
        puts "-before [month year or day month year ]  - key word search for record before a certain month and year or day month year"
        puts "-after [month year or day month year ]  - key word search for record after a certain month and year or day month year"
        puts "-before -after [month year of the before month and year][month year of the after month and year]  - key word search for record between a certain month and year or day month year"
    end

    def listing_record(options,key)
        if options == "--name"
            $record.search_first_name(key)
        elsif options == "--ip"
            $record.search_ip_address(key)
        end
    end

end

class Record

        def search_first_name(name)
            $doc.xpath('//record').each do 
                |record_element|
                    if record_element.xpath('first_name').text.downcase == name.chomp.downcase
                        record_output = {
                        :Id =>record_element.xpath('id').text,
                        :First_name  => record_element.xpath('first_name').text, 
                        :Last_name => record_element.xpath('last_name').text,
                        :Email => record_element.xpath('email').text,
                        :Gender =>record_element.xpath('gender').text,
                        :Ip_address =>record_element.xpath('ip_address').text,
                        :Send_date =>+record_element.xpath('send_date').text,
                        :Email_body =>+record_element.xpath('email_body').text,
                        :Email_title =>+record_element.xpath('email_title').text
                        }
                        puts JSON.pretty_generate(record_output)
                    end
                end 
        end
    
        def search_ip_address(name)
            $doc.xpath('//record').each do 
                |record_element|
                    if record_element.xpath('ip_address').text == name.chomp
                        record_output = {
                        :Id =>record_element.xpath('id').text,
                        :First_name  => record_element.xpath('first_name').text, 
                        :Last_name => record_element.xpath('last_name').text,
                        :Email => record_element.xpath('email').text,
                        :Gender =>record_element.xpath('gender').text,
                        :Ip_address =>record_element.xpath('ip_address').text,
                        :Send_date =>+record_element.xpath('send_date').text,
                        :Email_body =>+record_element.xpath('email_body').text,
                        :Email_title =>+record_element.xpath('email_title').text
                        }
                        puts JSON.pretty_generate(record_output)
                    end
                end 
        end

        def search_before_date(date)
            $doc.xpath('//record').each do 
                |record_element|
                    
                    comparison = Date.parse(date)
            
                    send_date = Date.parse record_element.xpath('send_date').text
                    if send_date < comparison
                            record_output = {
                            :Id =>record_element.xpath('id').text,
                            :First_name  => record_element.xpath('first_name').text, 
                            :Last_name => record_element.xpath('last_name').text,
                            :Email => record_element.xpath('email').text,
                            :Gender =>record_element.xpath('gender').text,
                            :Ip_address =>record_element.xpath('ip_address').text,
                            :Send_date =>+record_element.xpath('send_date').text,
                            :Email_body =>+record_element.xpath('email_body').text,
                            :Email_title =>+record_element.xpath('email_title').text
                            }
                            puts JSON.pretty_generate(record_output)
                    end     
            end  
        end

        def search_after_date(date)
            $doc.xpath('//record').each do 
                |record_element|
                    
                    comparison = Date.parse(date)
            
                    send_date = Date.parse record_element.xpath('send_date').text
                    if send_date > comparison
                            record_output = {
                            :Id =>record_element.xpath('id').text,
                            :First_name  => record_element.xpath('first_name').text, 
                            :Last_name => record_element.xpath('last_name').text,
                            :Email => record_element.xpath('email').text,
                            :Gender =>record_element.xpath('gender').text,
                            :Ip_address =>record_element.xpath('ip_address').text,
                            :Send_date =>+record_element.xpath('send_date').text,
                            :Email_body =>+record_element.xpath('email_body').text,
                            :Email_title =>+record_element.xpath('email_title').text
                            }
                            puts JSON.pretty_generate(record_output)
                    end     
            end  
        end

        def search_day(key)
            $doc.xpath('//record').each do 
                |record_element|
                    
                    comparison = key
            
                    date = Date.parse record_element.xpath('send_date').text
                    days = Date::DAYNAMES[date.wday]
            
                    if days.downcase == comparison.chomp.downcase
                            record_output = {
                            :Id =>record_element.xpath('id').text,
                            :First_name  => record_element.xpath('first_name').text, 
                            :Last_name => record_element.xpath('last_name').text,
                            :Email => record_element.xpath('email').text,
                            :Gender =>record_element.xpath('gender').text,
                            :Ip_address =>record_element.xpath('ip_address').text,
                            :Send_date =>+record_element.xpath('send_date').text,
                            :Email_body =>+record_element.xpath('email_body').text,
                            :Email_title =>+record_element.xpath('email_title').text
                            }
                            puts JSON.pretty_generate(record_output)
                    end       
            end 
        end
        
        def search_between(key1, key2)
            $doc.xpath('//record').each do 
                |record_element|

                before_date = Date.parse(key1)
                after_date = Date.parse(key2)

                send_date = Date.parse record_element.xpath('send_date').text
                if send_date < before_date  && send_date > after_date
                    record_output = {
                    :Id =>record_element.xpath('id').text,
                    :First_name  => record_element.xpath('first_name').text, 
                    :Last_name => record_element.xpath('last_name').text,
                    :Email => record_element.xpath('email').text,
                    :Gender =>record_element.xpath('gender').text,
                    :Ip_address =>record_element.xpath('ip_address').text,
                    :Send_date =>+record_element.xpath('send_date').text,
                    :Email_body =>+record_element.xpath('email_body').text,
                    :Email_title =>+record_element.xpath('email_title').text
                    }
                    puts JSON.pretty_generate(record_output)
                end
            end
        end     
                


    end

option = ARGV[0]

function = Function.new

$record = Record.new

if option == "-xml"
    filename = "nothing"
    if ARGV[1]== nil
        filename = "emails.xml"
    else
        filename = ARGV[1]
    end 
    function.read_file(filename)

elsif option == "list"
    xmlfile = File.read("emails.xml")
    $doc = Nokogiri::XML.parse(xmlfile)
    function.listing_record(ARGV[1],ARGV[2])

elsif option == "-before" && ARGV[1] =="-after"
    xmlfile = File.read("emails.xml")
    $doc = Nokogiri::XML.parse(xmlfile)
    key1 = ARGV[2] + ARGV[3]
    key2 = ARGV[4] + ARGV[5]
    $record.search_between(key1,key2)

elsif option == "-before"                                       #assuming that the date have more than just  the year 
    xmlfile = File.read("emails.xml")
    $doc = Nokogiri::XML.parse(xmlfile)
    if ARGV[3] == nil
        key = ARGV[1] + ARGV[2]
    else
        key = ARGV[1] + ARGV[2] + ARGV[3]
    end
    $record.search_before_date(key)

elsif option == "-after"                                        #assuming that the date have the month and the year 
    xmlfile = File.read("emails.xml")
    $doc = Nokogiri::XML.parse(xmlfile)
    if ARGV[3] == nil
        key = ARGV[1] + ARGV[2]
    else
        key = ARGV[1] + ARGV[2] + ARGV[3]
    end
    $record.search_after_date(key)

elsif option == "-day"
    xmlfile = File.read("emails.xml")
    $doc = Nokogiri::XML.parse(xmlfile)
    key = ARGV[1] 
    $record.search_day(key)

elsif option == "help"
    function.help

elsif option == nil
    puts "Commands"
    puts "s123456_p2.rb -xml[filename]"
    puts "s123456_p2.rb help"

end